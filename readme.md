# 1. 介绍

本项目是`frp`内网穿透工具精简出来的版本

通过命令下载 
`git clone https://gitee.com/nwu_zjq/frp && chmod 777 frp -R && cd frp`


## 1.1 修改配置

1. 修改服务端(云服务端)

通过修改`frp_*/frps.ini`

```shell
[common]
bind_port = 云服务器frp端口入口 # 7000
```

然后通过命令运行后台运行
`mkdir -p log/ && nohup ./frps -c frps.ini 1>log/out.txt 2>log/err.txt &`
也可以直接前台运行
`./frps -c frps.ini`


2. 修改客户端(本地服务器)

通过修改`frp_*/frpc.ini`

```shell
[common]
server_addr = 云服务器地址 # 1.1.26.26
server_port = 云服务器frp端口入口 # 7000
privilege_token = 密码 # 123456
login_fail_exit = false


[ssh]
type = tcp
local_ip = 127.0.0.1
local_port = 本地ssh端口出口 # 22
remote_port = 云服务器将本地ssh端口出口 # 7001
```

然后通过命令运行后台运行
`mkdir -p log/ && nohup ./frpc -c frpc.ini 1>log/out.txt 2>log/err.txt &`
也可以直接前台运行
`./frpc -c frpc.ini`

3. 如果正常, 这时候就可以通过远程ssh进行连接本地服务器了
`ssh -p 7001 账户@1.1.26.26`

# 2. 设置开机自启动

## 2.1 服务端(云服务器上)

1. 修改`frps.service` 里面的路径

```shell
[Unit]
Description=fraps client service
After=network.target syslog.target
Wants=network.target

[Service]
Type=simple
# 只需要修改这里的路径
ExecStart=<frp_root_path>/frp_arm/frps -c <frp_root_path>/frp_arm/frps.ini

[Install]
WantedBy=multi-user.target
```

2. 将开机启动服务文件放到system下
`sudo cp frps.service /lib/systemd/system/frps.service`

3. 测试效果
```shell
sudo systemctl start frps # 然后就启动frps
sudo systemctl enable frps # 再打开开机自启动
sudo systemctl restart frps # 重启服务
sudo systemctl stop frps # 停止服务
sudo systemctl status frps # 查看服务日志
```

## 2.1 客户端 (本地服务器)

1. 修改`frpc.service` 里面的路径

```shell
[Unit]
Description=frpc client service
After=network.target syslog.target
Wants=network.target

[Service]
Type=simple
# 只需要修改这里的路径
ExecStart=<frp_root_path>/frp_arm/frpc -c <frp_root_path>/frp_arm/frpc.ini

[Install]
WantedBy=multi-user.target
```

2. 将开机启动服务文件放到system下
`sudo cp frpc.service /lib/systemd/system/frpc.service`

3. 测试效果
```shell
sudo systemctl start frpc # 然后就启动frpc
sudo systemctl enable frpc # 再打开开机自启动
sudo systemctl restart frpc # 重启服务
sudo systemctl stop frpc # 停止服务
sudo systemctl status frpc # 查看服务日志
```







